package com.wyw.springdemo.plugins.scheduler;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.lang.reflect.Method;

public class CommonJob implements Job{

	private String clazz;
	private String method;
	private String cron;
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		try {
			Object obj = Class.forName(clazz).newInstance();
			Class<?> c = obj.getClass();
			Method m = c.getDeclaredMethod(method, null);
			m.invoke(obj, null);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public String getClazz() {
		return clazz;
	}
	public void setClazz(String clazz) {
		this.clazz = clazz;
	}
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	public String getCron() {
		return cron;
	}
	public void setCron(String cron) {
		this.cron = cron;
	}

}
