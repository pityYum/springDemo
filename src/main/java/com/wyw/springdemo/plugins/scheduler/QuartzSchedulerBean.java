package com.wyw.springdemo.plugins.scheduler;

import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

@Configuration
public class QuartzSchedulerBean {
	private static Logger logger = LoggerFactory.getLogger(QuartzSchedulerBean.class);

	/**
	 * SchedulerFactory调度器
	 */
	private SchedulerFactory schedulerFactory;
	/**
	 * schedule调度器
	 */
	private Scheduler schedule;

	/**
	 * 调度任务配置文件
	 */
	private String jobConfigFile;
	/**
	 * 调度任务配置文件
	 */
	private String quartzConfigFile;
	private static String CLAZZ = "clazz";
	private static String METHOD = "method";
	private static String CRON = "cron";
	
	public QuartzSchedulerBean() {
		this.jobConfigFile = "quartz_job.properties";
		this.quartzConfigFile = "quartz.properties";
	}
	
	public QuartzSchedulerBean(String jobConfigFile) {
		this.jobConfigFile = jobConfigFile;
		this.quartzConfigFile = "quartz.properties";
	}
	/**
	 * @param jobConfigFile 调度任务配置文件
	 * @param quartzConfigFile 定时器配置文件
	 */
	public QuartzSchedulerBean(String jobConfigFile, String quartzConfigFile) {
		this.jobConfigFile = jobConfigFile;
		this.quartzConfigFile = quartzConfigFile;
	}
	private void loadJobsFromConfigFile() {
		// 获取job配置文件
		Prop jobProp = PropKit.use(this.jobConfigFile);
		// 获得所有任务名
		Set<String> jobNames = this.getJobNamesFromProp(jobProp);
		// 逐个加载任务
		for (String jobName : jobNames) {
			JobBean jobBean = createJobBean(jobProp,jobName);
			loadJob(jobBean, jobName);
		}
	}
	
	private JobBean createJobBean(Prop jobProp, String jobName) {
		JobBean jobBean = new JobBean();
		jobBean.setJobId(UUID.randomUUID().toString());
		jobBean.setJobGroup(jobProp.get(jobName + ".group"));
		jobBean.setJobDesc(jobProp.get(jobName + ".desc"));
		
		jobBean.setJobClass(jobProp.get(jobName + ".class"));
		jobBean.setJobMethod(jobProp.get(jobName + ".method"));
		jobBean.setCronExpression(jobProp.get(jobName + ".cron"));
		jobBean.setEnable(jobProp.getBoolean(jobName + ".enable"));
		return jobBean;
	}

	/**
	 * @Title: getJobNamesFromProp
	 * @Description: 获得所有任务名
	 * @param jobProp
	 *            job配置
	 * @return 任务名集合
	 * @since V1.0.0
	 */
	private Set<String> getJobNamesFromProp(Prop jobProp) {
		Map<String, Boolean> jobNames = new HashMap<String, Boolean>();
		for (Object item : jobProp.getProperties().keySet()) {
			String fullKeyName = item.toString();
			// 获得job名
			String jobName = fullKeyName.substring(0, fullKeyName.indexOf("."));
			jobNames.put(jobName, Boolean.TRUE);
		}
		return jobNames.keySet();
	}
	
	/**
	 * @Title: loadJob
	 * @Description: 加载一个任务
	 * @param job
	 *            jobBean
	 * @param jobName
	 *            job名
	 * @since V1.0.0
	 */
	public void loadJob(JobBean job, String jobName) {
		// 任务开关，默认开启
		Boolean enable = job.isEnable();
		// 任务被禁用，直接返回
		if (!enable) {
			return;
		}
		try{
			TriggerKey triggerKey = TriggerKey.triggerKey(jobName);
			CronTrigger trigger = (CronTrigger) schedule.getTrigger(triggerKey);
			// 不存在，创建一个
			if (null == trigger) {
				JobDataMap jobDataMap = new JobDataMap();
				jobDataMap.put(CLAZZ, job.getJobClass());
				jobDataMap.put(METHOD, job.getJobMethod());
				jobDataMap.put(CRON, job.getCronExpression());
				
				JobDetail jobDetail = JobBuilder.newJob(CommonJob.class).usingJobData(jobDataMap).build();
				
				// 表达式调度构建器
				CronScheduleBuilder scheduleBuilder = CronScheduleBuilder.cronSchedule(job.getCronExpression());
				// 按新的cronExpression表达式构建一个新的trigger
				trigger = TriggerBuilder.newTrigger().withIdentity(jobName)
						.withSchedule(scheduleBuilder).build();
//				trigger = new CronTriggerImpl(jobDataMap.getString(METHOD)+UUID.randomUUID(),null,jobDataMap.getString(CRON));
				schedule.scheduleJob(jobDetail, trigger);
				
			} else {
				// Trigger已存在，那么更新相应的定时设置
				// 表达式调度构建器
				CronScheduleBuilder scheduleBuilder = CronScheduleBuilder.cronSchedule(job.getCronExpression());
				// 按新的cronExpression表达式重新构建trigger
				trigger = trigger.getTriggerBuilder().withIdentity(triggerKey).withSchedule(scheduleBuilder).build();
				// 按新的trigger重新设置job执行
				schedule.rescheduleJob(triggerKey, trigger);
			}
			logger.info("load job[{}],class[{}],method[{}],cron[{}]",jobName,job.getJobClass(),job.getJobMethod(),job.getCronExpression());
		}catch(Exception e){
			logger.error(e.getMessage(),e);
		}
	}

	@Bean
	public boolean start() {
		try {
			schedulerFactory = new StdSchedulerFactory(quartzConfigFile);
			schedule = schedulerFactory.getScheduler();
			if (this.jobConfigFile != null) {
				loadJobsFromConfigFile();
			}
			schedule.start();
		} catch (SchedulerException e) {
			e.printStackTrace();
		}
		logger.info("SchedulerPlugin is started");
		return true;
	}

	public boolean stop() {
		try {
			schedule.standby();
		} catch (SchedulerException e) {
			e.printStackTrace();
		}
		return false;
	}

}
