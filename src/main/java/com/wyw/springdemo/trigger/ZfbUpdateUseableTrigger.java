package com.wyw.springdemo.trigger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 支付宝账户，每天重置可用状态
 */
public class ZfbUpdateUseableTrigger {

	private static Logger logger = LoggerFactory.getLogger(ZfbUpdateUseableTrigger.class);
	public void callback() {
		long t1 = System.currentTimeMillis();
		logger.info("================开始执行支付宝账户可用状态重置定时任务=================");
		Thread t = new Thread(new Runnable() {
			@Override
			public void run() {
				System.out.println("update........................");
			}
		});
		t.start();
		long t2 = System.currentTimeMillis();
		logger.info("================结束支付宝账户可用状态重置定时任务,启动线程个数[{}],耗时[{}ms]=================",1,t2-t1);
	}
	
}
