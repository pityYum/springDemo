package com.wyw.springdemo.model;

import com.jfinal.kit.PathKit;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.generator.Generator;
import com.jfinal.plugin.hikaricp.HikariCpPlugin;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * 在数据库表有任何变动时，运行一下 main 方法，极速响应变化进行代码重构
 */
public class _YsbGenerator {

	public static HikariCpPlugin createDruidPlugin() {
		return new HikariCpPlugin(PropKit.get("spring.datasource.url"), PropKit.get("spring.datasource.username"), PropKit.get("spring.datasource.password").trim());
	}
	
	public static DataSource getDataSource() {
		PropKit.use("env/local/application-local.properties");
		HikariCpPlugin plugin = createDruidPlugin();
		plugin.start();
		return plugin.getDataSource();
	}
	
	public static void main(String[] args) {
		// base model 所使用的包名
		String baseModelPackageName = "com.wyw.springdemo.model.base";
		// base model 文件保存路径
		String baseModelOutputDir = PathKit.getWebRootPath() + "/src/main/java/com/wyw/springdemo/model/base";
		
		// model 所使用的包名 (MappingKit 默认使用的包名)
		String modelPackageName = "com.wyw.springdemo.model";
		// model 文件保存路径 (MappingKit 与 DataDictionary 文件默认保存路径)
		String modelOutputDir = baseModelOutputDir + "/..";
		
		// 创建生成器
		Generator generator = new Generator(getDataSource(), baseModelPackageName, baseModelOutputDir, modelPackageName, modelOutputDir);
		// 设置是否生成链式 setter 方法
		generator.setGenerateChainSetter(false);
		// 添加不需要生成的表名(前缀和后缀)
		generator.addExcludedTable(getExcTab("act_"));
		generator.addExcludedTable(getExcTab("cms_"));
		generator.addExcludedTable(getExcTab("gen_"));
		generator.addExcludedTable(getExcTab("oa_"));
		generator.addExcludedTable(getExcTab("rpt_"));
		generator.addExcludedTable(getExcTab("test_"));
		generator.addExcludedTable(getExcTab("_his"));
		generator.addExcludedTable(getExcTab("_t1"));
		generator.addExcludedTable(getExcTab("_settle"));
		generator.addExcludedTable("sys_user");
		generator.addExcludedTable("sys_role");
		generator.addExcludedTable("sys_menu");
		generator.addExcludedTable("sys_role_menu");
		generator.addExcludedTable("sys_role_office");
		generator.addExcludedTable("sys_user_role");
		generator.addExcludedTable("sys_dict");
		generator.addExcludedTable("sys_log");
		generator.addExcludedTable("sys_user_routeway");
		generator.addExcludedTable("bu_allrouteway_settle_flag");
		generator.addExcludedTable("bu_diff_detail");
		generator.addExcludedTable("bu_settle_log");
		// 设置是否在 Model 中生成 dao 对象
		generator.setGenerateDaoInModel(true);
		// 设置是否生成链式 setter 方法
		generator.setGenerateChainSetter(true);
		// 设置是否生成字典文件
		generator.setGenerateDataDictionary(false);
		// 设置需要被移除的表名前缀用于生成modelName。例如表名 "osc_user"，移除前缀 "osc_"后生成的model名为 "User"而非 OscUser
		generator.setRemovedTableNamePrefixes("bu_","tb_","sys_","t_");
		// 生成
		generator.generate();
	}
	
	private static String[] getExcTab(String preName){  
        String sql="SELECT table_name from information_schema.tables WHERE table_name LIKE '"+preName+"%' or table_name LIKE '%"+preName+"'" ;  
        List<String> list = new ArrayList<String>();  
        Connection conn = null;  
        try {  
            conn = getDataSource().getConnection();  
            Statement stmt = conn.createStatement();  
            ResultSet rs=stmt.executeQuery(sql);  
            while (rs.next()) {  
                list.add(rs.getString(1));  
            }  
        } catch (SQLException e) {  
            e.printStackTrace();  
        }finally{  
            try {  
                conn.close();  
            } catch (SQLException e) {  
                e.printStackTrace();  
            }  
        }  
        String[] s=new String[list.size()];  
        for (int i = 0; i < list.size(); i++) {  
            s[i]= list.get(i);  
        }  
        return s;  
    } 
}




