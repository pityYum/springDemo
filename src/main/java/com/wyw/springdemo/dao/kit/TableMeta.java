package com.wyw.springdemo.dao.kit;

import java.beans.BeanInfo;
import java.util.HashMap;
import java.util.Map;

public class TableMeta {

    private String tableName;
    /**主键javaBean**/
    private String primaryKey;
    /**主键dbColumn**/
    private String primaryColumn;
    /**key:value-->javaBean:dbColumn**/
    private Map<String,String> columns = new HashMap<String,String>();
    private BeanInfo beanInfo;
    private Class<?> modelClass;

    public String getPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(String primaryKey) {
        this.primaryKey = primaryKey;
    }

    public Class<?> getModelClass() {
        return modelClass;
    }

    public void setModelClass(Class<?> modelClass) {
        this.modelClass = modelClass;
    }

    public BeanInfo getBeanInfo() {
        return beanInfo;
    }

    public void setBeanInfo(BeanInfo beanInfo) {
        this.beanInfo = beanInfo;
    }

    public Map<String, String> getColumns() {
        return columns;
    }

    public void setColumns(Map<String, String> columns) {
        this.columns = columns;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getPrimaryColumn() {
        return primaryColumn;
    }

    public void setPrimaryColumn(String primaryColumn) {
        this.primaryColumn = primaryColumn;
    }
}
