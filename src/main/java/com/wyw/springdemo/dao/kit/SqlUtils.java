package com.wyw.springdemo.dao.kit;

import com.wyw.springdemo.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.support.KeyHolder;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

/**
 * sql辅助为类
 *
 * User: liyd
 * Date: 2/13/14
 * Time: 10:03 AM
 */
public class SqlUtils {
    /** 日志对象 */
    private static final Logger LOG = LoggerFactory.getLogger(SqlUtils.class);
    /**
     * 构建insert语句
     *
     * @param entity 实体映射对象
     * @return
     */
    public static SqlContext buildInsertSql(Object entity,TableMeta tm) throws Exception {
        Class<?> clazz = entity.getClass();
        String tableName = tm.getTableName();
        String primaryName = tm.getPrimaryColumn();
        StringBuilder sql = new StringBuilder("insert into ");
        List<Object> params = new ArrayList<Object>();
        sql.append(tableName);
        //获取属性信息
        BeanInfo beanInfo = tm.getBeanInfo();
        PropertyDescriptor[] pds = beanInfo.getPropertyDescriptors();
        sql.append("(");
        StringBuilder args = new StringBuilder();
        args.append("(");
        for (PropertyDescriptor pd : pds) {
            Object value = getReadMethodValue(pd.getReadMethod(), entity);
            if (value == null) {
                continue;
            }
            sql.append(tm.getColumns().get(pd.getName()));
            args.append("?");
            params.add(value);
            sql.append(",");
            args.append(",");
        }
        sql.deleteCharAt(sql.length() - 1);
        args.deleteCharAt(args.length() - 1);
        args.append(")");
        sql.append(")");
        sql.append(" values ");
        sql.append(args);
        return new SqlContext(sql, primaryName, params);
    }
    /**
     * 构建更新sql
     *
     * @param entity
     * @return
     */
    public static SqlContext buildUpdateSql(Object entity,TableMeta tm) throws Exception {
        Class<?> clazz = entity.getClass();
        StringBuilder sql = new StringBuilder();
        List<Object> params = new ArrayList<Object>();
        String tableName = tm.getTableName();//nameHandler.getTableName(clazz.getSimpleName());
        String primaryName = tm.getPrimaryColumn();//nameHandler.getPrimaryName(clazz.getSimpleName());
        //获取属性信息
        BeanInfo beanInfo = tm.getBeanInfo();//ClassUtils.getSelfBeanInfo(clazz);
        PropertyDescriptor[] pds = beanInfo.getPropertyDescriptors();
        sql.append("update ");
        sql.append(tableName);
        sql.append(" set ");
        Object primaryValue = null;
        for (PropertyDescriptor pd : pds) {
            Object value = getReadMethodValue(pd.getReadMethod(), entity);
            if (value == null) {
                continue;
            }
            String columnName = tm.getColumns().get(pd.getName());//nameHandler.getColumnName(pd.getName());
            if (primaryName.equalsIgnoreCase(columnName)) {
                primaryValue = value;
            }
            sql.append(columnName);
            sql.append(" = ");
            sql.append("?");
            params.add(value);
            sql.append(",");
        }
        sql.deleteCharAt(sql.length() - 1);
        sql.append(" where ");
        sql.append(primaryName);
        sql.append(" = ?");
        params.add(primaryValue);
        return new SqlContext(sql, primaryName, params);
    }
    /**
     * 构建查询条件
     *  @param entity
     *
     */
    public static SqlContext buildQueryCondition(Object entity,TableMeta tm) throws Exception {
        //获取属性信息
        BeanInfo beanInfo = tm.getBeanInfo();//ClassUtils.getSelfBeanInfo(entity.getClass());
        // PropertyDescriptor[] pds = BeanUtils.getPropertyDescriptors(entityClass);
        PropertyDescriptor[] pds = beanInfo.getPropertyDescriptors();
        StringBuilder condition = new StringBuilder();
        List<Object> params = new ArrayList<Object>();
        int count = 0;
        for (PropertyDescriptor pd : pds) {
            Object value = getReadMethodValue(pd.getReadMethod(), entity);
            if (value == null) {
                continue;
            }
            if (count > 0) {
                condition.append(" and ");
            }
            condition.append(tm.getColumns().get(pd.getName()));
            condition.append(" = ?");
            params.add(value);
            count++;
        }
        return new SqlContext(condition, null, params);
    }
    /**
     * 获取属性值
     *
     * @param readMethod
     * @param entity
     * @return
     */
    private static Object getReadMethodValue(Method readMethod, Object entity) throws Exception {
        if (readMethod == null) {
            return null;
        }
        try {
            if (!Modifier.isPublic(readMethod.getDeclaringClass().getModifiers())) {
                readMethod.setAccessible(true);
            }
            return readMethod.invoke(entity);
        } catch (Exception e) {
            LOG.error("获取属性值失败", e);
            throw new Exception(e);
        }
    }
    private static Object doWriteMethodValue(Method writeMethod, Object entity, Object key) throws Exception {
        if (writeMethod == null) {
            return null;
        }
        try {
            if (!Modifier.isPublic(writeMethod.getDeclaringClass().getModifiers())) {
                writeMethod.setAccessible(true);
            }
            return writeMethod.invoke(entity,key);
        } catch (Exception e) {
            LOG.error("写入属性值失败", e);
            throw new Exception(e);
        }
    }
    public static void fillPrimaryValue(KeyHolder keyHolder, Object entity, TableMeta tm) throws Exception {
        BeanInfo beanInfo = tm.getBeanInfo();
        PropertyDescriptor[] pds = beanInfo.getPropertyDescriptors();
        for (PropertyDescriptor pd : pds) {
            if (tm.getPrimaryKey().equals(pd.getName())) {
                Number key = keyHolder.getKey();
                Object value = key;
                if(pd.getPropertyType().equals(Integer.class)){
                    value = key.intValue();
                }else if(pd.getPropertyType().equals(Long.class)){
                    value = key.longValue();
                }else if(pd.getPropertyType().equals(Float.class)){
                    value = key.floatValue();
                }else if(pd.getPropertyType().equals(Double.class)){
                    value = key.doubleValue();
                }
                doWriteMethodValue(pd.getWriteMethod(), entity, value);
                break;
            }
        }
    }

    public static void main(String[] args) throws IntrospectionException {
        User user = new User();
        BeanInfo beanInfo = Introspector.getBeanInfo(user.getClass(), user.getClass().getSuperclass());
        PropertyDescriptor[] pds = beanInfo.getPropertyDescriptors();
        for (PropertyDescriptor pd : pds) {
            System.out.println(pd.getName());
            System.out.println(pd.getPropertyType().equals(String.class));
        }

    }
}
