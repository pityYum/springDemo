package com.wyw.springdemo.dao.base;

import com.wyw.springdemo.configuration.TableCache;
import com.wyw.springdemo.dao.kit.Page;
import com.wyw.springdemo.dao.kit.SqlContext;
import com.wyw.springdemo.dao.kit.SqlUtils;
import com.wyw.springdemo.dao.kit.TableMeta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * 泛型通用dao实现 依赖于spring jdbcTemplate
 *
 */
public abstract class BaseDaoImpl<T> implements BaseDao<T> {

    /** 具体操作的实体类对象 */
    private Class<T> entityClass;

    private TableMeta tm;

    /** spring jdbcTemplate 对象 */
    @Autowired
    protected JdbcTemplate jdbcTemplate;

    /**
     * 构造方法，获取运行时的具体实体对象
     */
    public BaseDaoImpl() {
        Type superclass = getClass().getGenericSuperclass();
        ParameterizedType type = (ParameterizedType) superclass;
        entityClass = (Class<T>) type.getActualTypeArguments()[0];
        tm = TableCache.getTables().get(entityClass);
    }

    /**
     * 插入一条记录
     *
     * @param entity
     */
    @Override
    public Long insert(T entity) throws Exception {
        final SqlContext sqlContext = SqlUtils.buildInsertSql(entity,tm);
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(new PreparedStatementCreator() {
            public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                PreparedStatement ps = con.prepareStatement(sqlContext.getSql().toString(),
                        new String[] { sqlContext.getPrimaryKey() });
                int index = 0;
                for (Object param : sqlContext.getParams()) {
                    index++;
                    ps.setObject(index, param);
                }
                return ps;
            }
        }, keyHolder);
        SqlUtils.fillPrimaryValue(keyHolder,entity,tm);
        return keyHolder.getKey().longValue();
    }

    /**
     * 更新记录
     *
     * @param entity
     */
    @Override
    public void update(T entity) throws Exception {
        SqlContext sqlContext = SqlUtils.buildUpdateSql(entity,tm);
        jdbcTemplate.update(sqlContext.getSql().toString(), sqlContext.getParams().toArray());
    }

    /**
     * 删除记录
     *
     * @param id
     */
    @Override
    public void delete(Serializable id) {
        String tableName = tm.getTableName();
        String primaryName = tm.getPrimaryKey();
        String sql = new StringBuilder().append("DELETE FROM ").append(tableName).append(" WHERE ").append(primaryName).append(" = ?").toString();
        jdbcTemplate.update(sql, id);
    }

    /**
     * 删除所有记录
     */
    @Override
    public void deleteAll() {
        String tableName = tm.getTableName();
        String sql = " TRUNCATE TABLE " + tableName;
        jdbcTemplate.execute(sql);
    }

    /**
     * 得到记录
     *
     * @param id
     * @return
     */
    @Override
    public T getById(Serializable id) {
        String tableName = tm.getTableName();
        String primaryName = tm.getPrimaryKey();
        StringBuffer sql = new StringBuffer("SELECT * FROM ");
        sql.append(tableName).append(" WHERE ").append(primaryName).append(" = ?");
        return (T) jdbcTemplate.queryForObject(sql.toString(),new BeanPropertyRowMapper<T>(entityClass));
    }

    /**
     * 查询所有记录
     *
     * @return
     */
    @Override
    public List<T> findAll() {
        String sql = "SELECT * FROM " + tm.getTableName();
        return (List<T>) jdbcTemplate.query(sql,new BeanPropertyRowMapper<T>(entityClass));
    }

    /**
     * 查询记录数
     *
     * @param entity
     * @return
     */
    public int queryCount(T entity) throws Exception {
        String tableName = tm.getTableName();
        StringBuilder countSql = new StringBuilder("select count(*) from ");
        countSql.append(tableName);
        SqlContext sqlContext = SqlUtils.buildQueryCondition(entity,tm);
        if (sqlContext.getSql().length() > 0) {
            countSql.append(" where ");
            countSql.append(sqlContext.getSql());
        }
        return jdbcTemplate.queryForObject(countSql.toString(), sqlContext.getParams().toArray(),Integer.class);
    }

    protected static class Holder {
        // "order\\s+by\\s+[^,\\s]+(\\s+asc|\\s+desc)?(\\s*,\\s*[^,\\s]+(\\s+asc|\\s+desc)?)*";
        private static final Pattern ORDER_BY_PATTERN = Pattern.compile(
                "order\\s+by\\s+[^,\\s]+(\\s+asc|\\s+desc)?(\\s*,\\s*[^,\\s]+(\\s+asc|\\s+desc)?)*",
                Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
    }
    public String replaceOrderBy(String sql) {
        return Holder.ORDER_BY_PATTERN.matcher(sql).replaceAll("");
    }
    /**
     * 查询分页列表
     *
     * @return
     */
    public Page queryPageList(int pageNumber, int pageSize, String select, String sqlExceptSelect,Class<?> entityClass,Object... objects) throws Exception {
        StringBuilder countSql = new StringBuilder("select count(*) ").append(replaceOrderBy(sqlExceptSelect));

        Long totalRow = jdbcTemplate.queryForObject(countSql.toString(), objects,Long.class);
        if (totalRow == 0) {
            return new Page(new ArrayList(0), pageNumber, pageSize, 0, 0);	// totalRow = 0;
        }
        int totalPage = (int) (totalRow / pageSize);
        if (totalRow % pageSize != 0) {
            totalPage++;
        }
        int limit = (pageNumber-1)*pageSize;
        StringBuilder querySql = new StringBuilder(select).append(' ').append(sqlExceptSelect);
        querySql.append(" limit ").append(limit).append(',').append(pageSize);
        List list;
        if (entityClass == null) {
            list = jdbcTemplate.queryForList(querySql.toString(),objects);
        }else{
            list = jdbcTemplate.query(querySql.toString(), objects,new BeanPropertyRowMapper<>(entityClass));
        }
        return new Page(list, pageNumber, pageSize, totalPage, totalRow.intValue());
    }
}
