package com.wyw.springdemo.dao.base;

import java.io.Serializable;
import java.util.List;

public interface BaseDao<T> {

    public Long insert(T entity) throws Exception;
    public void update(T entity) throws Exception;
    public void delete(Serializable id) ;
    public void deleteAll();
    public T getById(Serializable id);
    public List<T> findAll();
}
