package com.wyw.springdemo.configuration;

import com.wyw.springdemo.filter.MyFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MyConfiguration {
    @Bean
    public FilterRegistrationBean addFilter(){
        FilterRegistrationBean fr = new FilterRegistrationBean();
        fr.setFilter(new MyFilter());
        fr.addUrlPatterns("/*");
        fr.addInitParameter("paramName","paramValue");
        fr.setName("myfilter");
        fr.setOrder(1);
        return fr;
    }
}
