package com.wyw.springdemo.configuration;

import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.template.source.ClassPathSourceFactory;
import com.wyw.springdemo.model._MappingKit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;


@Configuration
public class ActiveRecordPluginConfig {

    @Value("${spring.datasource.username}")
    private String username;
    @Value("${spring.datasource.password}")
    private String password;
    @Value("${spring.datasource.url}")
    private String url;

    @Autowired
    private DataSource dataSource;

    @Bean
    public ActiveRecordPlugin initActiveRecordPlugin() {

//        DruidPlugin druidPlugin = new DruidPlugin(url, username, password);
        // 加强数据库安全
//        WallFilter wallFilter = new WallFilter();
//        wallFilter.setDbType("mysql");
//        druidPlugin.addFilter(wallFilter);
        // 添加 StatFilter 才会有统计数据
        // druidPlugin.addFilter(new StatFilter());
        // 必须手动调用start
//        druidPlugin.start();

//        HikariCpPlugin hikariCpPlugin = new HikariCpPlugin(url, username, password);
//        hikariCpPlugin.start();
        ActiveRecordPlugin arp = new ActiveRecordPlugin(dataSource);
//        arp.setTransactionLevel(Connection.TRANSACTION_READ_COMMITTED);
        _MappingKit.mapping(arp);
        arp.setShowSql(false);

        arp.getEngine().setSourceFactory(new ClassPathSourceFactory());
        // 必须手动调用start
        arp.start();
        return arp;
    }

}

