package com.wyw.springdemo.configuration;

import com.wyw.springdemo.common.utils.BeanUtils;
import com.wyw.springdemo.dao.kit.TableMeta;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class TableCache {
	private static final String scanPackage = "com.wyw.springdemo.model";
	private static Map<Class<?>, TableMeta> tables = new HashMap<Class<?>, TableMeta>();
	public static Map<Class<?>, TableMeta> getTables(){
        return tables;
    }
	static{
		Set<Class<?>> classSet = BeanUtils.getClasses(scanPackage);
		for(Class<?> clazz:classSet){
			Table tableAnno = clazz.getAnnotation(Table.class);
			if(tableAnno!=null){
				try{
					TableMeta tm = new TableMeta();
					tm.setTableName(tableAnno.name());
					tm.setModelClass(clazz);
					tm.setBeanInfo(Introspector.getBeanInfo(clazz, clazz.getSuperclass()));
					PropertyDescriptor[] properties = tm.getBeanInfo().getPropertyDescriptors();
					Map<String,String> columns = new HashMap<String,String>();
					for (PropertyDescriptor property:properties) {
						Method method = property.getReadMethod();
						Column colAnno = method.getAnnotation(Column.class);
						if(colAnno!=null){
							columns.put(property.getName(),colAnno.name());
							Id idAnno = method.getAnnotation(Id.class);
							if (null != idAnno) {
								tm.setPrimaryKey(property.getName());
								tm.setPrimaryColumn(colAnno.name());
							}
						}
					}
					tm.setColumns(columns);
					tables.put(clazz, tm);
				}catch(Exception e){
				}
			}
		}
	}

}
