package com.wyw.springdemo.controller;

import com.wyw.springdemo.constant.MyProperties;
import com.wyw.springdemo.dao.kit.Page;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@Controller
@EnableAutoConfiguration
public class HelloSpringBoot {
    @Resource
    public MyProperties pro;

    @RequestMapping("/index")
    public String index(Page page, HttpServletRequest request){
        System.out.println(pro.getName());
        String s = (String) request.getSession().getAttribute("sid");
        String sid = request.getRequestedSessionId();
        request.getSession().setAttribute("sid", sid);
        return "index";
    }

    @RequestMapping("test")
    public ModelAndView test(){
        return new ModelAndView("test");
    }
    @RequestMapping("login")
    public ModelAndView login(){
        return new ModelAndView("login");
    }
}
