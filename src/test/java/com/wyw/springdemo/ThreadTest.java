package com.wyw.springdemo;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
public class ThreadTest {

    @Async("taskExecutor")
    public void doThread(){
        System.out.println("thread----test");
    }
}
