package com.wyw.springdemo;

import com.wyw.springdemo.dao.UserDao;
import com.wyw.springdemo.plugins.scheduler.QuartzSchedulerBean;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringdemoApplicationTests {


    @Autowired
    private UserDao userDao;
    @Resource
    private ThreadTest threadTest;
    @Resource
    private QuartzSchedulerBean quartzSchedulerPlugin;

    @Test
    public void contextLoads() throws Exception {

    }

}
